"""
Program Name: sedy
Author      : Mustafa TEKPINAR
Copyright   : Mustafa TEKPINAR - 2021-2024
Email       : tekpinar@buffalo.edu
Purpose     : A Python toolkit to investigate relations between protein structures, evolution and dynamics.
"""

__all__ = ['dfi', 'schlitter', 'rmsf', 'bfactor', 'cfb', 'postprocess', 'compare', 'convert', 'conservation', 'loadit', 'charge', 'hydropathy', 'wcn']

__version__ = '0.1.3'
