import sys
from prody import *
import numpy as np

from sedy.postprocess import *

def loaditApp(args):

    #Parse the protein structures.
    structure = parsePDB(args.pdb)
    #calphas = structure.select('name CA')
    calphas = structure.select('all')
    N = calphas.numAtoms()

    chidsList = calphas.getChids()
    resnumsList = calphas.getResnums()
    bfactor = calphas.getBetas()

    print("@> Reading data from the text file!")
    data = np.genfromtxt(args.input)

    print(len(data.T[args.column-1]))
    #Let's check if length of two datasets are identical.
    #More precisely, if number Calphas are equal to the 
    #number of data points in the data file. 
    if(N==len(data.T[args.column-1])):
        rawData = data.T[args.column-1]
        print("@> Writing data to outfile.pdb!")

        if(args.field2load=='beta'):
            calphas.setBetas(rawData*args.scaler)
        elif(args.field2load=='occupancy'):
            calphas.setOccupancies(rawData*args.scaler)
        else:
            print("ERROR: Unknown --field2load or -f option!")
            print("       It can be only beta or occupancy!")
            sys.exit(-1)
    else:
        print("@> WARNING: Number of calphas is not equal to the number "+\
            "data points in the text file!")
        #Reset all calpha bfactors to zero
        if(args.field2load=='beta'):
            calphas.setBetas(np.zeros(N))
            for atom in calphas:
                resnum=(atom.getResnum())
                rawData = data.T[args.column-1]
                resIDs = data.T[0]
                if (resnum in resIDs):
                    index = np.where(resIDs == resnum)
                    #index = resIDs.index(resnum)
                    atom.setBeta(rawData[index]*args.scaler)

            writePDB(args.outfile, calphas)
        elif(args.field2load=='occupancy'):
            calphas.setOccupancies(np.zeros(N))
            for atom in calphas:
                resnum=(atom.getResnum())
                rawData = data.T[args.column-1]
                resIDs = data.T[0]
                if (resnum in resIDs):
                    index = np.where(resIDs == resnum)
                    #index = resIDs.index(resnum)
                    atom.setOccupancy(rawData[index]*args.scaler)

            writePDB(args.outfile, calphas)            
        else:
            print("ERROR: Unknown --field2load or -f option!")
            print("       It can be only beta or occupancy!")
            sys.exit(-1)



        
    # if(args.process == 'minmax'):
    #     bfactor = minMaxNormalization(bfactor)
    # elif(args.process == '1-minmax'):
    #     bfactor = 1.0 - minMaxNormalization(bfactor)
    # elif(args.process == '1-values'):
    #     bfactor = 1.0 - bfactor
    # elif(args.process == 'ranksort'):
    #     bfactor = rankSortData(bfactor)
    # elif(args.process == '1-ranksort'):
    #     bfactor = 1.0 - rankSortData(bfactor)
    # elif(args.process == 'zscore'):
    #     bfactor = zscore(bfactor)
    # elif(args.process == 'negzscore'):
    #     bfactor = -1.0*zscore(bfactor)
    # elif(args.process == None):
    #     print("@> No postprocessing applied!")
    # else:
    #     print("@> Unknown postprocessing option!")
    #     print("@> Available postprocessing options:")
    #     print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
    #     sys.exit(-1)

    # if(args.outfile != ""):
    #     with open(args.outfile, 'w') as file:
    #         for i in range (N):
    #             file.write("{:d} {:6.2f} {:s} \n".format(resnumsList[i], \
    #                                                     bfactor[i], \
    #                                                     chidsList[i]))

    print("@> loadit finished successfully!")
