import sys
from prody import *
import numpy as np
import MDAnalysis as mda
from MDAnalysis.analysis import align
from MDAnalysis.analysis.rms import RMSF

from sedy.postprocess import *

def minMaxNormalization(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

def calculateRmsf(N, calphas, covMatrix, \
    normalized=True, outfile="rmsf.dat"):
    """
        Calculate RMSF
        from a coarse-grained elastic network model.

        N: Number of calpha atoms 
        calphas: prody object of ca atoms.
        covMatrix: Covariance Matrix

    Return:
    ------
        rmsf: an numpy array    
        The array has length N and contains rmsf entropy values for all aa's.  

    """

    M = np.zeros((3, 3))
    I = np.zeros((3, 3))
    np.fill_diagonal(I, 1.0)

    coeff1 = 1.0 #kTe^2/(hbar^2)
    coeff2 = 1.0 #0.5*k

    #for i in range (N):
    rmsfList = []
    # import matplotlib.pyplot as plt
    # plt.figure()
    # plt.plot(1.0 - np.array(rmsfList))
    # plt.show()
        #print(sigma)
    if(normalized):
        rmsfList = minMaxNormalization(rmsfList)

    if(outfile != ""):
        with open(outfile, 'w') as file:
            for i in range (N):
                file.write("{:d} {:s} {:.6f}\n".format(calphas[i].getResnum(), \
                                calphas[i].getChid(), rmsfList[i]))

    return np.array(rmsfList)

def rmsfApp(args):

    if(args.trajectory == None):
        #Parse the protein structures.
        structure = parsePDB(args.pdb)
        calphas = structure.select('name CA')
        N = calphas.numAtoms()

        #Calculate Covariance
        anm = ANM('ANM')
        anm.buildHessian(calphas)
        if(args.nmodes==None):
            anm.calcModes(n_modes='all')
        else:
            anm.calcModes(n_modes=int(args.nmodes))
        
        chidsList = calphas.getChids()
        resnumsList = calphas.getResnums()
        
        # covMatrix = calcCovariance(anm)
        print("@> Calculating {} using normal modes!".format(args.fluctuationtype))
        rmsf = calcSqFlucts(anm)
        if(args.fluctuationtype=='rmsf'):
            rmsf = np.sqrt(rmsf)

    else:
        print("@> Calculating {} using the molecular dynamics trajectory!".format(args.fluctuationtype))
        # Create the universe (That sounds really fancy :)
        universe = mda.Universe(args.pdb, args.trajectory)

        # Create an atomgroup from the alpha carbon selection
        calphas = universe.select_atoms("protein and name CA")
        
        N = calphas.n_atoms

        chidsList = calphas.chainIDs
        resnumsList = calphas.resnums

        # chidList = []
        # for i in range(0, N):
        #     chidList.append(calphas[i].chainID)

        print(f"@> Parsed {N} Calpha atoms.")
        # Set your frame window for your trajectory that you want to analyze
        # startingFrame = 0
        # if endingFrame == -1:
        #     endingFrame = universe.trajectory.n_frames
        # skip = 1 

        alignTrajectory = True
        # Perform Calpha alignment first
        if alignTrajectory:
            print("@> Aligning only Calpha atoms to the initial frame!")
            alignment = align.AlignTraj(universe, universe, select="protein and name CA", in_memory=False)
            alignment.run()

        rmsfer = RMSF(calphas, verbose=True).run()
        rmsf = rmsfer.results.rmsf
        if(args.fluctuationtype=='msf'):
            rmsf = np.square(rmsf)
        # print(rmsf)

    if(args.process == 'minmax'):
        rmsf = minMaxNormalization(rmsf)
    elif(args.process == '1-minmax'):
        rmsf = 1.0 - minMaxNormalization(rmsf)
    elif(args.process == '1-values'):
        rmsf = 1.0 - rmsf
    elif(args.process == 'ranksort'):
        rmsf = rankSortData(rmsf)
    elif(args.process == '1-ranksort'):
        rmsf = 1.0 - rankSortData(rmsf)
    elif(args.process == 'zscore'):
        rmsf = zscore(rmsf)
    elif(args.process == 'negzscore'):
        rmsf = -1.0*zscore(rmsf)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        print("@> Available postprocessing options:")
        print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
        sys.exit(-1)

    if(args.outfile != ""):
        if(args.outfile.lower().endswith(('.pdb'))):
            calphas.setBetas(rmsf)
            writePDB(args.outfile, calphas)
        else:
            with open(args.outfile, 'w') as file:
                file.write("#Resid Value Chain\n")
                for i in range (N):
                    file.write("{:d} {:.6f} {:s} \n".format(resnumsList[i], \
                                                            rmsf[i], \
                                                            chidsList[i]))

    print("@> Finished {} calculation!".format(args.fluctuationtype))
