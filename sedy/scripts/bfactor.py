import sys
from prody import *
import numpy as np
import scipy.stats

from sedy.postprocess import *

def bfactorApp(args):

    #Parse the protein structures.
    structure = parsePDB(args.pdb)
    calphas = structure.select('name CA')
    N = calphas.numAtoms()

    chidsList = calphas.getChids()
    resnumsList = calphas.getResnums()
    bfactor = calphas.getBetas()

    # covMatrix = calcCovariance(anm)
    print("@> Processing bfactors!")

    if(args.process == 'minmax'):
        bfactor = minMaxNormalization(bfactor)
    elif(args.process == '1-minmax'):
        bfactor = 1.0 - minMaxNormalization(bfactor)
    elif(args.process == '1-values'):
        bfactor = 1.0 - bfactor
    elif(args.process == 'ranksort'):
        bfactor = rankSortData(bfactor)
    elif(args.process == '1-ranksort'):
        bfactor = 1.0 - rankSortData(bfactor)
    elif(args.process == 'zscore'):
        bfactor = zscore(bfactor)
    elif(args.process == 'negzscore'):
        bfactor = -1.0*zscore(bfactor)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        print("@> Available postprocessing options:")
        print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
        sys.exit(-1)

    if(args.outfile != ""):
        with open(args.outfile, 'w') as file:
            file.write("#Resid Value Chain\n")
            for i in range (N):
                file.write("{:d} {:6.2f} {:s} \n".format(resnumsList[i], \
                                                        bfactor[i], \
                                                        chidsList[i]))

    print("@> Finished bfactor calculation!")
