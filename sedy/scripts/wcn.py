import sys
from prody import *
from sedy.postprocess import *
import numpy as np

def minMaxNormalization(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

# from correlationplus.calculate import *
# from correlationplus.centralityAnalysis import *
def calculate_geometric_center(atoms):
    coordinates = atoms.getCoords()
    return np.mean(coordinates, axis=0)

def calculate_WCN_alphaside(protein):
    """
    Calculate the side-chain weighted contact number (WCN^αρ_ρ) for each residue.
    
    Parameters:
    protein (prody.AtomGroup): The protein structure loaded with ProDy
    
    Returns:
    numpy.ndarray: An array of WCN^αρ_ρ values for each residue
    """
    
    protein_ca = protein.select('protein and name CA')
    n_residues = protein_ca.numAtoms()  # Using numAtoms() instead of numResidues()
    WCN_side_chain = np.zeros(n_residues)
    print("Number of residues!")
    print(n_residues)
    for i in range(n_residues):
        residue_i = protein_ca[i].getResindex()
        full_residue_i = protein.select(f'same residue as index {residue_i}')
        
        side_chain_i = full_residue_i.select('sidechain')
        #side_chain_i = full_residue_i.select('not backbone and not hydrogen')
        if side_chain_i is None or side_chain_i.numAtoms() == 0:
            center_i = full_residue_i.select('name CA').getCoords()[0]
        else:
            center_i = side_chain_i.getCoords().mean(axis=0)
        dist_alpha = 0.0
        dist_side = 0.0
        for j in range(n_residues):
            if i != j:
                residue_j = protein_ca[j].getResindex()
                full_residue_j = protein.select(f'same residue as index {residue_j}')
                
                # Calculate distance to alpha carbon
                alpha_carbon_j = full_residue_j.select('name CA')
                dist_alpha = np.linalg.norm(center_i - alpha_carbon_j.getCoords()[0])
                
                side_chain_j = full_residue_j.select('sidechain')
                #side_chain_j = full_residue_j.select('not backbone and not hydrogen')
                if side_chain_j is None or side_chain_j.numAtoms() == 0:
                    center_j = alpha_carbon_j.getCoords()[0]
                else:
                    center_j = side_chain_j.getCoords().mean(axis=0)
                
                dist_side = np.linalg.norm(center_i - center_j)

                if dist_alpha > 0:
                    WCN_side_chain[i] += 1 / (dist_alpha ** 2)
                if dist_side > 0:
                    WCN_side_chain[i] += 1 / (dist_side ** 2)
    
    return WCN_side_chain


############################################################################################
# WCN from Lindorf-Larsenn
from scipy.spatial.distance import pdist, squareform
#import prody as pd

def get_residue_coordinates(structure):
    """
    Get coordinates for each residue, using side chain heavy atoms or CA for glycine.
    
    Parameters:
    structure (prody.AtomGroup): ProDy AtomGroup of the protein structure
    
    Returns:
    dict: Dictionary with residue indices as keys and coordinate arrays as values
    """
    coords_dict = {}
    for residue in structure.iterResidues():
        resindex = residue.getResindex()
        if residue.getResname() == 'GLY':
            # For glycine, use CA atom
            atom = residue.select('name CA')
        else:
            # For other residues, use side chain heavy atoms
            atom = residue.select('(not backbone) and (not hydrogen)')
        
        if atom:
            coords_dict[resindex] = atom.getCoords().mean(axis=0)
    
    return coords_dict

def calculate_wcn(coords_dict, r0=7.0):
    """
    Calculate the Weighted Contact Number (WCN) for each residue in a protein structure.
    
    Parameters:
    coords_dict (dict): Dictionary of residue coordinates
    r0 (float): Distance parameter, default is 7.0 Å
    
    Returns:
    dict: Dictionary of WCN values for each residue
    """
    residue_indices = list(coords_dict.keys())
    coordinates = np.array([coords_dict[i] for i in residue_indices])
    
    # Calculate pairwise distances
    distances = squareform(pdist(coordinates))
    
    # Calculate s(r) for all pairs
    s_r = (1 - (distances / r0)**6) / (1 - (distances / r0)**12)
    
    # Set diagonal elements to 0 (exclude self-interactions)
    np.fill_diagonal(s_r, 0)
    
    # Sum s(r) for each residue to get WCN
    wcn_values = np.sum(s_r, axis=1)
    
    #return dict(zip(residue_indices, wcn_values))
    return wcn_values

def calculate_wcn_from_pdb_larsenn(pdb_file):
    """
    Calculate WCN values from a PDB file.
    
    Parameters:
    pdb_file (str): Path to the PDB file
    
    Returns:
    dict: Dictionary of WCN values for each residue
    """
    # Load the structure
    structure = parsePDB(pdb_file)
    
    # Get coordinates for each residue
    coords_dict = get_residue_coordinates(structure)
    
    # Calculate WCN
    wcn_values = calculate_wcn(coords_dict)
    
    return wcn_values

# # Example usage
# if __name__ == "__main__":
#     pdb_file = "path/to/your/pdb/file.pdb"  # Replace with actual PDB file path
#     wcn_values = calculate_wcn_from_pdb_larsenn(pdb_file)
    
#     print("WCN values:")
#     for resindex, wcn in wcn_values.items():
#         print(f"Residue {resindex}: {wcn}")
############################################################################################
def calculate_wcn_sidechain(structure):
    """Calculate the Weighted Contact Number (WCN) for each residue in a protein structure.
    
    Parameters:
    coords_dict (dict): Dictionary of residue coordinates
    r0 (float): Distance parameter, default is 7.0 Å
    
    Returns:
    dict: Dictionary of WCN values for each residue
    """

    coords_dict = get_residue_coordinates(structure)
    residue_indices = list(coords_dict.keys())
    coordinates = np.array([coords_dict[i] for i in residue_indices])
    
    # Calculate pairwise distances
    distances = squareform(pdist(coordinates))
    
    # Set diagonal elements to inf to avoid division by zero erro
    np.fill_diagonal(distances, np.inf)
    
    # Calculate WCN
    wcn_values = np.sum(1 / distances**2, axis=1)
    
    #return dict(zip(residue_indices, wcn_values))
    return wcn_values
    
def wcnApp(args):
    """
        WCN is weighted contact number. It is a structural feature.
    """
    calphas = parsePDB(args.pdb, subset='ca')
    protein = parsePDB(args.pdb)

    distanceMatrix = buildDistMatrix(calphas)

    chidsList = calphas.getChids()
    resnumsList = calphas.getResnums()

    N = calphas.numAtoms()
    wcnList = []
    if(args.type == 'calpha'):
        for i in range(0, N):
            wcn = 0.0
            for j in range(0, N):
                if(i!=j):
                    wcn += (1.0/(distanceMatrix[i][j]*distanceMatrix[i][j]))
            wcnList.append(wcn)
    if(args.type == 'sidechain'):
        #Calculate sidechain geometrical center of each amino acid. 
        #####################################
        # Calculate WCN^αρ_ρ
        WCN_values = calculate_wcn_sidechain(protein)
        wcnList = list(WCN_values)
        #####################################
    if(args.type == 'larsenn'):
        #Calculate WCN from Larsenn's paper! 
        #####################################
        # Calculate WCN^αρ_ρ
        # Get coordinates for each residue
        coords_dict = get_residue_coordinates(protein)
    if(args.type == 'alphaside'):
        #Equation 9 of Echave. 
        #####################################
        WCN_values = calculate_WCN_alphaside(protein)
        wcnList = list(WCN_values)
        print(len(wcnList))        

    if(args.process == 'minmax'):
        wcnList = minMaxNormalization(wcnList)
    elif(args.process == '1-minmax'):
        wcnList = 1.0 - minMaxNormalization(wcnList)
    elif(args.process == '1-values'):
        wcnList = 1.0 - wcnList
    elif(args.process == 'ranksort'):
        wcnList = rankSortData(wcnList)
    elif(args.process == '1-ranksort'):
        wcnList = 1.0 - rankSortData(wcnList)
    elif(args.process == 'zscore'):
        wcnList = zscore(wcnList)
    elif(args.process == 'negzscore'):
        wcnList = -1.0*zscore(wcnList)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        print("@> Available postprocessing options:")
        print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
        sys.exit(-1)
    

    if(args.outfile != ""):
        if(args.outfile.lower().endswith(('.pdb'))):
            calphas.setBetas(wcnList)
            writePDB(args.outfile, calphas)
        else:
            with open(args.outfile, 'w') as file:
                file.write("#Resid Value Chain\n")
                for i in range (N):
                    file.write("{:d} {:.6f} {:s} \n".format(resnumsList[i], \
                                                            wcnList[i], \
                                                            chidsList[i]))

    print("@> Finished WCN calculation!")