import sys
from prody import *
import numpy as np

from sedy.postprocess import *

one_letter ={'VAL':'V', 'ILE':'I', 'LEU':'L', 'GLU':'E', 'GLN':'Q', \
            'ASP':'D', 'ASN':'N', 'HIS':'H', 'TRP':'W', 'PHE':'F', 'TYR':'Y',    \
            'ARG':'R', 'LYS':'K', 'SER':'S', 'THR':'T', 'MET':'M', 'ALA':'A',    \
            'GLY':'G', 'PRO':'P', 'CYS':'C'}

#  Amino acid hydropathy.
# hydropathyDict = {'A': 1.8, 'C': 2.5, 'D': -3.5, 'E': -3.5,
#             'F': 2.8, 'G': -0.4, 'H': -3.2, 'I': 4.5,
#             'K': -3.9, 'L': 3.8, 'M': 1.9, 'N': -3.5,
#             'P': -1.6, 'Q': -3.5, 'R': -4.5, 'S': -0.8,
#             'T': -0.7, 'V': 4.2, 'W': -0.9, 'Y': -1.3}

# Black & Mould hydrophobicity index
# Anal. Biochem. 193:72-82(1991).
# "BlackMould"
bm = {"A": 0.616, "R": 0.0, "N": 0.236, "D": 0.028, "C": 0.68,
      "Q": 0.251, "E": 0.043, "G": 0.501, "H": 0.165, "I": 0.943,
      "L": 0.943, "K": 0.283, "M": 0.738, "F": 1.0, "P": 0.711,
      "S": 0.359, "T": 0.45, "W": 0.878, "Y": 0.88, "V": 0.825}

def minMaxNormalization(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

def hydropathyApp(args):

    #Parse the protein structures.
    structure = parsePDB(args.pdb)
    calphas = structure.select('name CA')
    N = calphas.numAtoms()
    
    chidsList = calphas.getChids()
    resnumsList = calphas.getResnums()
    
    print("@> Assigning hydropathy to amino acids using predefined Black & Mould hydrophobicity index!")
    hydropathy = N*[None]
    for i in range (N):
        # print(calphas[i].getResname())
        hydropathy[i] = bm[one_letter[calphas[i].getResname()]]


    if(args.process == 'minmax'):
        hydropathy = minMaxNormalization(hydropathy)
    elif(args.process == '1-minmax'):
        hydropathy = 1.0 - minMaxNormalization(hydropathy)
    elif(args.process == '1-values'):
        hydropathy = 1.0 - hydropathy
    elif(args.process == 'ranksort'):
        hydropathy = rankSortData(hydropathy)
    elif(args.process == '1-ranksort'):
        hydropathy = 1.0 - rankSortData(hydropathy)
    elif(args.process == 'zscore'):
        hydropathy = zscore(hydropathy)
    elif(args.process == 'negzscore'):
        hydropathy = -1.0*zscore(hydropathy)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        print("@> Available postprocessing options:")
        print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
        sys.exit(-1)

    if(args.outfile != ""):
        if(args.outfile.lower().endswith(('.pdb'))):
            calphas.setBetas(hydropathy)
            writePDB(args.outfile, calphas)
        else:
            with open(args.outfile, 'w') as file:
                file.write("#Resid Value Chain\n")
                for i in range (N):
                    file.write("{:d} {:.3f} {:s} \n".format(resnumsList[i], \
                                                            hydropathy[i], \
                                                            chidsList[i]))

    print("@> Finished the task of hydropathy assignments!")
