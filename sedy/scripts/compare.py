import sys
# from prody import *
import numpy as np
# from matplotlib.pylab import *
# from sedy.postprocess import *
from scipy.stats import spearmanr, pearsonr
from scipy.spatial import distance

def compareApp(args):

    #Read the first input file
    dataSet1 = np.genfromtxt(args.inputfile1)
    dataCol1 = dataSet1.T[args.icol-1]

    #Read the second input file
    dataSet2 = np.genfromtxt(args.inputfile2)
    dataCol2 = dataSet2.T[args.jcol-1]

    if(len(dataCol1) != (len(dataCol2))):
        print("@> Please make sure that the lengths of two datasets are equal.")
        sys.exit(-1)

    if(args.metric == 'bray-curtis'):
        #Please pay attention that we are converting distances to
        #similarities by subtracting them from 1.0.
        value = distance.braycurtis(dataCol1, dataCol2)
        print("@> Bray-Curtis Similarity: {}".format(value))
    elif(args.metric == 'bray-curtis-distance'):
        value = distance.braycurtis(dataCol1, dataCol2)
        print("@> Bray-Curtis Distance: {}".format(value))
    elif(args.metric == 'cosine'):
        #value = np.dot(dataCol1, dataCol2)/(np.linalg.norm(dataCol1)*np.linalg.norm(dataCol2))
        value = distance.cosine(dataCol1, dataCol2)
        print("@> Cosine Similarity: {}".format(1-value))
    elif(args.metric == 'spearman'):
        value, pvalue = spearmanr(dataCol1, dataCol2)
        print("@> Spearman Similarity: {:.3f} ; pvalue: {:.1E}".format(value, pvalue))
    elif(args.metric == 'pearson'):
        value, pvalue = pearsonr(dataCol1, dataCol2)
        print("@> Pearson Similarity: {:.3f} ; pvalue: {:.1E}".format(value, pvalue))
    elif(args.metric == 'euclidean'):
        value = distance.euclidean(dataCol1, dataCol2)
        print("@> Euclidean Distance: {}".format(value))
    else:
        print("@> Unknown comparison metric! It can only be one of these:")
        print("@> bray-curtis, cosine, spearman, pearson, euclidian.")
        sys.exit(-1)

    print("@> Comparison of the data finished!")

