from prody import *
from correlationplus.calculate import *
from correlationplus.centralityAnalysis import *

def cfbApp(args):
    selectedAtoms = parsePDB(args.pdb, subset='ca')
    valueFilter = float(args.vmin)
    distanceFilter = float(args.dmax)
    distanceMatrix = buildDistMatrix(selectedAtoms)

    if(args.trajectory == None):
        # Calculate dynamical correlations (ccMatrix) from 
        # a coarse-grained elastic network model.  


        if(args.nmodes==None):
            n_modes=3*(selectedAtoms.numAtoms()) - 6
        else:
            n_modes=int(args.nmodes)

        if (args.type == "ndcc"):
            ccMatrix = calcENMnDCC(selectedAtoms, args.rcutoff,
                        method="ANM", 
                        nmodes=n_modes,
                        normalized=True,
                        saveMatrix=True,
                        out_file=args.outfile)
        else:
            ccMatrix = calcENM_LMI(selectedAtoms, args.rcutoff,
                        method="ANM", 
                        nmodes=n_modes,
                        normalized=True,
                        saveMatrix=True,
                        out_file=args.outfile)
    else:
        # Calculate dynamical correlations (ccMatrix) from
        # an MD trajectory. 
        if args.type == "ndcc":
            ccMatrix = calcMDnDCC(args.pdb, args.trajectory,
                       startingFrame=args.beg,
                       endingFrame=args.end,
                       normalized=True,
                       alignTrajectory=True,
                       saveMatrix=True,
                       out_file=args.outfile)
        else:
            ccMatrix = calcMD_LMI(args.pdb, args.trajectory,
                       startingFrame=args.beg,
                       endingFrame=args.end,
                       normalized=True,
                       alignTrajectory=True,
                       atomSelection="(protein and name CA)",
                       saveMatrix=True,
                       out_file=args.outfile)


    network = buildDynamicsNetwork(ccMatrix, distanceMatrix, \
                            valueFilter, distanceFilter,\
                            selectedAtoms)
    centralityAnalysis(network, valueFilter, distanceFilter, args.outfile, "current_flow_betweenness",
                           selectedAtoms)