import sys
import argparse
from sedy import __version__ as cp_vers
from sedy.scripts.dfi import dfiApp
from sedy.scripts.schlitter import schlitterApp
from sedy.scripts.rmsf import rmsfApp
from sedy.scripts.bfactor import bfactorApp
from sedy.scripts.cfb import cfbApp
from sedy.scripts.postprocess import postprocessApp

from sedy.scripts.compare import compareApp

from sedy.scripts.conservation import conservationApp
from sedy.scripts.loadit import loaditApp
#from sedy.scripts.convert import convertApp

from sedy.scripts.charge import chargeApp
from sedy.scripts.hydropathy import hydropathyApp

from sedy.scripts.wcn import wcnApp
#TODO:

def usage_main():
    """
    Show how to use this program!
    """
    print("""
Example usage:
sedy -h
Sedy contains the following apps:
 - dfi
 - schlitter
 - rmsf
 - bfactor
 - cfb
 - plots
 - compare
 - convert
 - conservation
 - loadit
 - charge
 - hydropathy
 - wcn
You can get more information about each individual app as follows:
sedy dfi -h
sedy schlitter -h
sedy rmsf -h
sedy bfctor -h
sedy cfb -h
sedy plots -h
sedy compare -h
sedy convert -h
sedy conservation -h
sedy loadit -h
sedy charge -h
sedy hydropathy -h
sedy wcn -h
""")


def main():

    print("""

| sedy       :  A Python toolkit to investigate relations between protein sequences and dynamics. 
|                                                                                                 
| Copyright   (C) Mustafa Tekpinar 2021-2024                                                           
| Address      :  Department of Physics, Van YYU, 65080, Van, Turkey.                   
| Email        :  tekpinar@buffalo.edu                                                            
| Licence      :  GNU LGPL V3                                                                     
|                                                                                                 
| Documentation:                                                                                  
| Citation     : .................................................................................
| Version      : {0}                                                                            

""".format(cp_vers))

    main_parser = argparse.ArgumentParser()
    subparsers = main_parser.add_subparsers(dest='command')

    #dfi script argument parsing
    dfi_parser  = subparsers.add_parser('dfi')

    dfi_parser.add_argument("-p", "--pdb", dest="pdb", required=True, default=None)
    dfi_parser.add_argument("-n", "--nmodes", dest="nmodes", type=str,  required=False, default=None)
    dfi_parser.add_argument("-r", "--rcutoff", dest="rcutoff", type=float,  required=False, default=15.0)
    dfi_parser.add_argument("-b", "--beg", dest="beg", \
                            help="Frame number for the first frame (Default is 0).", \
                            type=int,  required=False, default=0)

    dfi_parser.add_argument("-e", "--end", dest="end", \
                            help="Frame number for the last frame (Default is -1,"+\
                            " which means the last frame in the trajectory.)", \
                            type=int,  required=False, default=-1)
    dfi_parser.add_argument("-t", "--trajectory", \
                                help='Trajectory file in xtc, trr or dcd format.',
                                dest="trajectory", type=str,  required=False, default=None)
    dfi_parser.add_argument("--process", dest="process", type=str,  required=False, default=None)
    dfi_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")

    #schlitter script argument parsing
    schlitter_parser  = subparsers.add_parser('schlitter')
    schlitter_parser.add_argument("-p", "--pdb", dest="pdb", required=True, default=None)
    schlitter_parser.add_argument("-n", "--nmodes", dest="nmodes", type=str,  required=False, default=None)
    schlitter_parser.add_argument("-r", "--rcutoff", dest="rcutoff", type=float,  required=False, default=15.0)
    schlitter_parser.add_argument("-b", "--beg", dest="beg", \
                            help="Frame number for the first frame (Default is 0).", \
                            type=int,  required=False, default=0)

    schlitter_parser.add_argument("-e", "--end", dest="end", \
                            help="Frame number for the last frame (Default is -1,"+\
                            " which means the last frame in the trajectory.)", \
                            type=int,  required=False, default=-1)
    schlitter_parser.add_argument("-t", "--trajectory", \
                                help='Trajectory file in xtc, trr or dcd format.',
                                dest="trajectory", type=str,  required=False, default=None)
    schlitter_parser.add_argument("--process", dest="process", type=str,  required=False, default=None)
    schlitter_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")

    #rmsf script argument parsing
    rmsf_parser  = subparsers.add_parser('rmsf')
    rmsf_parser.add_argument("-p", "--pdb", dest="pdb", required=True, default=None)
    rmsf_parser.add_argument("-n", "--nmodes", dest="nmodes", type=str,  required=False, default=None)
    rmsf_parser.add_argument("-r", "--rcutoff", dest="rcutoff", type=float,  required=False, default=15.0)
    rmsf_parser.add_argument("-b", "--beg", dest="beg", \
                            help="Frame number for the first frame (Default is 0).", \
                            type=int,  required=False, default=0)

    rmsf_parser.add_argument("-e", "--end", dest="end", \
                            help="Frame number for the last frame (Default is -1,"+\
                            " which means the last frame in the trajectory.)", \
                            type=int,  required=False, default=-1)

    rmsf_parser.add_argument("-t", "--trajectory", \
                             help='Trajectory file in xtc, trr or dcd format.',
                             dest="trajectory", type=str,  required=False, default=None)
    rmsf_parser.add_argument("--process", dest="process", type=str,  required=False, default=None)
    rmsf_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")
    rmsf_parser.add_argument("-f", "--fluctuationtype", dest="fluctuationtype", \
                            help="msf or rmsf (Default is rmsf)",\
                            required=False, default="rmsf")

    #bfactor script argument parsing
    bfactor_parser  = subparsers.add_parser('bfactor')
    bfactor_parser.add_argument("-p", "--pdb", dest="pdb", 
                                help="Name of your protein data bank file.",\
                                required=True, default=None)
    bfactor_parser.add_argument("--process", dest="process", \
                                help="One of these: minmax, 1-minmax, 1-values, zscore",\
                                type=str,  required=False, default=None)
    bfactor_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")

    #cfb script argument parsing
    cfb_parser  = subparsers.add_parser('cfb')
    cfb_parser.add_argument("-p", "--pdb", dest="pdb", required=True, default=None)
    cfb_parser.add_argument("-n", "--nmodes", dest="nmodes", type=str,  required=False, default=None)
    cfb_parser.add_argument("-r", "--rcutoff", dest="rcutoff", type=float,  required=False, default=15.0)
    cfb_parser.add_argument("-b", "--beg", dest="beg", \
                            help="Frame number for the first frame (Default is 0).", \
                            type=int,  required=False, default=0)

    cfb_parser.add_argument("-e", "--end", dest="end", \
                            help="Frame number for the last frame (Default is -1,"+\
                            " which means the last frame in the trajectory.)", \
                            type=int,  required=False, default=-1)
    cfb_parser.add_argument("-t", "--trajectory", 
                            help='Trajectory file in xtc, trr or dcd format.',
                            dest="trajectory", type=str,  required=False, default=None)
    cfb_parser.add_argument("--process", dest="process", \
                            help="One of these: minmax, 1-minmax, 1-values, zscore",\
                            type=str,  required=False, default=None)
    cfb_parser.add_argument("--vmin", dest="vmin", type=float,  required=False, default=0.3)
    cfb_parser.add_argument("--dmax", dest="dmax", type=float,  required=False, default=10.0)
    cfb_parser.add_argument("--type", dest="type", type=str,  required=False, default="nlmi")
    cfb_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")

    #postprocess script argument parsing
    postprocess_parser  = subparsers.add_parser('postprocess')
    postprocess_parser.add_argument("-i", "--input", \
        help="rmsf, dfi or schlitter file that contain at least two columns.", \
        dest="input", required=True, default=None)
    postprocess_parser.add_argument("-c", "--column", 
        help="An integer value for the data column to process.",
        dest="column", type=int,  required=False, default=2)
    postprocess_parser.add_argument("--process", \
        help="One of these options: ranksort, 1-ranksort, minmax, 1-minmax, 1-values, zscore, negzscore, attenuate", 
        dest="process", type=str,  required=False, default=None)
    postprocess_parser.add_argument("-o", "--outfile", 
        help="Name of your output file.", 
        dest="outfile", default="outfile.dat")

    # #conservation script argument parsing
    # conservation_parser  = subparsers.add_parser('cfb')
    # conservation_parser.add_argument('alignfile', help="alignfile must be in fasta or clustal format.")
    # conservation_parser.add_argument("-o", "--outfile", 
    #                                 help="Name of your output file.", 
    #                                 dest="outfile", default="outfile.dat")
    
    # #plots script argument parsing
    # plots_parser = subparsers.add_parser('plots')
    # plots_parser.add_argument('-i', '--inputfile', dest='inputfile', type=str, \
    #     help='One of the output files of gemme, rhapsody or evmutation', \
    #     required=True, default=None)
    # plots_parser.add_argument('-d', '--datatype', dest='datatype', type=str, \
    #     help='dfi, dfi, cfg (current flow betweenness) or schlitter', \
    #     required=False, default='gemme')
    # plots_parser.add_argument('-t', '--type', dest='type', type=str, \
    #     help='Type of the 2D data that you want to extract. \n It can be min or max.', \
    #     required=False, default=False)
    # plots_parser.add_argument('-o', '--outputfile', dest='outputfile', type=str, \
    #     help='Name of the output file.', \
    #     required=False, default='output.txt')


    #compare script argument parsing
    compare_parser = subparsers.add_parser('compare')
    compare_parser.add_argument('-i', '--inputfile1', dest='inputfile1', type=str, \
        help='One of the output files of methods such as dfi, rmsf, schlitter or cfb', \
        required=True, default=None)
    compare_parser.add_argument('--icol', dest='icol', type=int, \
        help='Which column in the first file contains the actual data.', \
        required=False, default=2)
    compare_parser.add_argument('-j', '--inputfile2', dest='inputfile2', type=str, \
        help='One of the output files of conservation methods.', \
        required=True, default=None)
    compare_parser.add_argument('--jcol', dest='jcol', type=int, \
        help='Which column in the second file contains the actual data.', \
        required=False, default=2)
    compare_parser.add_argument('-m', '--metric', dest='metric', type=str, \
        help='Comparison metric. It can be bray-curtis, cosine, euclidean, spearman or pearson. Default is bray-curtis.',\
        required=False, default="bray-curtis")
    # compare_parser.add_argument('-o', '--outputfile', dest='outputfile', type=str, \
    #     help='Name of the output file. Default is None', \
    #     required=False, default=None)

    # #convert script argument parsing
    # convert_parser = subparsers.add_parser('convert')
    # convert_parser.add_argument('-i', '--inputfile', dest='inputfile', type=str, \
    #     help='One of the output files of gemme, rhapsody or evmutation', \
    #     required=True, default=None)
    # convert_parser.add_argument('--itype', dest='itype', type=str, \
    #     help='gemme, rhapsody, foldx or evmutation. Default is gemme.', \
    #     required=False, default='foldx')
    # convert_parser.add_argument('-o', '--outputfile', dest='outputfile', type=str, \
    #     help='Default is gemme', \
    #     required=True, default='output.txt')
    # convert_parser.add_argument('--otype', dest='otype', type=str, \
    #     help='gemme or dms (standard format). Default is gemme.', \
    #     required=False, default='gemme')
    # convert_parser.add_argument('--aaorder', dest='aaorder', type=str, \
    #     help='Amino acid order as a single string. Default is alphabetical: \"ACDEFGHIKLMNPQRSTVWY\"', \
    #     required=False, default='ACDEFGHIKLMNPQRSTVWY')

    #conservation script argument parser
    conservation_parser = subparsers.add_parser('conservation')

    conservation_parser.add_argument('alignfile',\
        help="Name of the input Multiple Sequence Alignment file in fasta or clustal format.")
    
    conservation_parser.add_argument('-a', '--reference', dest='reference', type=str, \
        help="reference sequence. Print scores in reference to a specific sequence (ignoring gaps). Default prints the entire column.",
        required=False, default=None)

    conservation_parser.add_argument('-b', '--lamda', dest='lamda', type=float, \
        help="lambda for window heuristic linear combination. Default=.5 [real in [0,1]]",
        required=False, default=0.5)

    conservation_parser.add_argument('-d', '--distribution', dest='distribution', type=str, \
        help="background distribution file, e.g., swissprot.distribution. Default=BLOSUM62 background [filename]",
        required=False, default="blosum62.distribution")

    conservation_parser.add_argument('-g', '--gap', dest='gap', type=float, \
        help="gap cutoff. Do not score columns that contain more than gap cutoff fraction gaps. Default=.3 [real in [0, 1)]",
        required=False, default=0.3)

    conservation_parser.add_argument('-l', '--sequenceweight', dest='sequenceweight', type=bool, \
        help="use sequence weighting. Default=True [True|False]",
        required=False, default=True)

    conservation_parser.add_argument('-m', '--matrix', dest='matrix', type=str, \
        help="similarity matrix file, e.g., blosum62.bla or .qij. Default=blosum62.bla matrix [filename]",
        required=False, default="blosum62.bla")

    conservation_parser.add_argument('-n', '--normalize', dest='normalize', type=bool, \
        help="normalize scores. Print the z-score (over the alignment) of each column raw score. Default=False",
        required=False, default=False)

    conservation_parser.add_argument('-o', '--output', dest='output', type=str, \
        help="Name of the output conservation quantity. Default is output.dat",
        required=False, default="output.dat")

    conservation_parser.add_argument('-p', '--penalty', dest='penalty', type=bool, \
        help="use gap penalty. Lower the score of columns that contain gaps. Default=True [True|False]",
        required=False, default=True)
    
    conservation_parser.add_argument('-s', '--scoringmethod', dest='scoringmethod', type=str, \
        help="conservation estimation method. \n\t\tOptions: shannon_entropy, property_entropy, property_relative_entropy, vn_entropy, relative_entropy, js_divergence, sum_of_pairs. Default=js_divergence",
        required=False, default="js_divergence")

    conservation_parser.add_argument('-w', '--window', dest='window', type=int, \
        help="window size. Number of residues on either side included in the window. Default=3 [int]",
        required=False, default=3)

    #loadit script argument parsing
    loadit_parser  = subparsers.add_parser('loadit')
    loadit_parser.add_argument("-p", "--pdb", dest="pdb", 
        help="Name of your protein data bank file.",\
        required=True, default=None)
    loadit_parser.add_argument("-i", "--input", \
        help="rmsf, dfi, schlitter or any other data file that contains at least two columns.", \
        dest="input", required=True, default=None)
    loadit_parser.add_argument("-c", "--column", 
        help="An integer value for the data column of the text file (not pdb file).",
        dest="column", type=int,  required=False, default=2)
    loadit_parser.add_argument("-f", "--field2load", 
        help="The field to load the data. It can be beta or occupancy.",
        dest="field2load", type=str,  required=False, default='beta')    
    loadit_parser.add_argument("-s", "--scaler", 
        help="An float value to multiply with value in the data column of the text file (not pdb file).",
        dest="scaler", type=float,  required=False, default=1.00)
    loadit_parser.add_argument("-o", "--outfile", 
        help="Name of your output protein data bank file.", 
        dest="outfile", default="outfile.pdb")

    #charge script argument parsing
    charge_parser  = subparsers.add_parser('charge')
    charge_parser.add_argument("-p", "--pdb", dest="pdb", 
                                help="Name of your protein data bank file.",\
                                required=True, default=None)
    charge_parser.add_argument("--process", dest="process", \
                                help="One of these: minmax, 1-minmax, 1-values, zscore",\
                                type=str,  required=False, default=None)
    charge_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")

    #hydropathy script argument parsing
    hydropathy_parser  = subparsers.add_parser('hydropathy')
    hydropathy_parser.add_argument("-p", "--pdb", dest="pdb", 
                                help="Name of your protein data bank file.",\
                                required=True, default=None)
    hydropathy_parser.add_argument("--process", dest="process", \
                                help="One of these: minmax, 1-minmax, 1-values, zscore",\
                                type=str,  required=False, default=None)
    hydropathy_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")

    #wcn script argument parsing
    wcn_parser  = subparsers.add_parser('wcn')
    wcn_parser.add_argument("-p", "--pdb", dest="pdb", required=True, default=None)
    wcn_parser.add_argument("--process", dest="process", \
                            help="One of these: minmax, 1-minmax, 1-values, zscore",\
                            type=str,  required=False, default=None)
    wcn_parser.add_argument("--type", dest="type", type=str, required=False, \
                            help='This parameter can be calpha or sidechain', default="calpha")
    wcn_parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")    

    args = main_parser.parse_args()

    if args.command == "dfi":
        dfiApp(args)
    elif args.command == "schlitter":
        schlitterApp(args)
    elif args.command == "rmsf":
        rmsfApp(args)
    elif args.command == "bfactor":
        bfactorApp(args)
    elif args.command == "cfb":
        cfbApp(args)
    elif args.command == "postprocess":
        postprocessApp(args)
    # elif args.command == "plots":
    #     plotsApp(args)
    elif args.command == "compare":
       compareApp(args)
    elif args.command == "conservation":
       conservationApp(args)
    elif args.command == "loadit":
       loaditApp(args)
    elif args.command == "charge":
       chargeApp(args)
    elif args.command == "hydropathy":
       hydropathyApp(args)
    # elif args.command == "-h" or args.command == "--help":
    #     usage_main()
    elif args.command == "wcn":
        wcnApp(args)
    else:
        usage_main()
        sys.exit(-1)
    
if __name__ == "__main__":
    main()
