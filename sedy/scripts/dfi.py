import sys
from prody import *
import numpy as np
import MDAnalysis as mda
from MDAnalysis.analysis import align
from sedy.postprocess import *

def calcperturbMat(invHrs, direct, resnum, Normalize=True):
    """
    Calculates perturbation matrix used for dfi calculation.

    # This calcperturbMat function belongs to Avishek Kumar. 
    # Its licence is BSD 3-clause license. The code is in public domain  
    # and can be accessed at https://github.com/avishekrk/DFI
    Input
    -----
    invHRS: numpy matrix
       covariance matrix (3N,3N), where N is the number of residues
    direct: numpy matrix
       matrix of peturbation directions
    resnum: int
       number of residues in protein
    Normalize: bool
       Normalize peturbation matrix
    Output
    ------
    peturbMat: numpy matrix
       NxN peturbation matrix where N is the number of residues
    """
    perturbMat = np.zeros((resnum, resnum))
    for k in range(len(direct)):
        peturbDir = direct[k, :]
        for j in range(int(resnum)):
            delforce = np.zeros(3 * resnum)
            delforce[3 * j:3 * j + 3] = peturbDir
            delXperbVex = np.dot(invHrs, delforce)
            delXperbMat = delXperbVex.reshape((resnum, 3))
            delRperbVec = np.sqrt(np.sum(delXperbMat * delXperbMat, axis=1))
            perturbMat[:, j] += delRperbVec[:]
    perturbMat /= 7

    if(Normalize):
        nrmlperturbMat = perturbMat / np.sum(perturbMat)
    else:
        print("WARNING: The perturbation matrix is not NORMALIZED")
        nrmlperturbMat = perturbMat

    return nrmlperturbMat
def dfiApp(args):

    # confProDy(auto_show=False)
    if(args.trajectory == None):
        structure = parsePDB(args.pdb)
        calphas = structure.select('name CA')
        N = calphas.numAtoms()
        resnamesList = calphas.getResnames()
        chidsList = calphas.getChids()
        resnumsList = calphas.getResnums()

        anm = ANM('ANM')
        anm.buildHessian(calphas)
        if(args.nmodes==None):
            anm.calcModes(n_modes='all')
        else:
            anm.calcModes(n_modes=int(args.nmodes))

        print("@> Calculating DFI...")
        dfi = calcDynamicFlexibilityIndex(anm, calphas, select='ca', norm=True)
        print("@> Finished DFI calculation!")

    else:
        print("@> Calculating the covariance matrix using the molecular dynamics trajectory!")
        # Create the universe (That sounds really fancy :)
        universe = mda.Universe(args.pdb, args.trajectory)

        # Create an atomgroup from the alpha carbon selection
        calphas = universe.select_atoms("protein and name CA")        
        N = calphas.n_atoms

        #Align the trtajectory
        alignTrajectory = True
        # Perform Calpha alignment first
        if alignTrajectory:
            print("@> Aligning only Calpha atoms to the initial frame!")
            alignment = align.AlignTraj(universe, universe, select="protein and name CA", in_memory=False)
            alignment.run()
        resnamesList = calphas.resnames
        chidsList = calphas.chainIDs
        resnumsList = calphas.resnums


        Rvector = []
        skip = 1
        # Iterate through the universe trajectory
        for timestep in universe.trajectory[args.beg:args.end:skip]:
            Rvector.append(calphas.positions.flatten())
        ##############################################

        # I reassign this bc in lmiMatrix calculation, we may skip some part of the trajectory!
        N_Frames = len(Rvector)

        R_average = np.mean(Rvector, axis=0)
        print("@> Calculating linear mutual information matrix from MD trajectory:")

        ################### Covariance matrix calculation part!    
        fullCovarMatrix = np.zeros((3 * N, 3 * N), np.double)
        for k in range(0, len(Rvector)):
            if k % 100 == 0:
                print("@> Frame: " + str(k))
            deltaR = np.subtract(Rvector[k], R_average)
            fullCovarMatrix += np.outer(deltaR, deltaR)

        # Do the averaging
        covMatrix = fullCovarMatrix / float(N_Frames)
        # This part of the code below here till the end of the else 
        # statement belongs to Avishek Kumar. 
        # Its licence is BSD 3-clause license. The code is in public domain  
        # and can be accessed at https://github.com/avishekrk/DFI
        directions = np.vstack(([1, 0, 0], [0, 1, 0], [0, 0, 1], [
                            1, 1, 0], [1, 0, 1], [0, 1, 1], [1, 1, 1]))
        normL = np.linalg.norm(directions, axis=1)
        direct = directions / normL[:, None]
        nrmlperturbMat = calcperturbMat(covMatrix, direct, N)
        dfi = np.sum(nrmlperturbMat, axis=1)

    if(args.process == 'minmax'):
        dfi = minMaxNormalization(dfi)
    elif(args.process == '1-minmax'):
        dfi = 1.0 - minMaxNormalization(dfi)
    elif(args.process == '1-values'):
        dfi = 1.0 - dfi
    elif(args.process == 'ranksort'):
        dfi = rankSortData(dfi)
    elif(args.process == '1-ranksort'):
        dfi = 1.0 - rankSortData(dfi)
    elif(args.process == 'zscore'):
        dfi = zscore(dfi)
    elif(args.process == 'negzscore'):
        dfi = -1.0*zscore(dfi)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        print("@> Available postprocessing options:")
        print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
        sys.exit(-1)


    with open(args.outfile, 'w') as f:
        f.write("#Resid Value Chain\n")
        for i in range (len(dfi)):
            f.write("{:d} {:.6f} {:s} \n".format(resnumsList[i], \
                                dfi[i], chidsList[i]))
    sys.exit()

