import sys
# from prody import *
import numpy as np
# from matplotlib.pylab import *
from sedy.postprocess import *
import pandas as pd

def postprocessApp(args):
    df = pd.read_table(args.input, sep="\s+")

    #data = np.genfromtxt(args.input,dtype=None)
    data = df.to_numpy()

    # print(data)
    rawData = data.T[args.column-1]
    # print(rawData)
    # processedData = rankdata(rawData.T[args.column - 1])/float(len(rawData.T[args.column - 1]))

    if(args.process == 'ranksort'):
        processedData = rankSortData(rawData)
    elif(args.process == '1-ranksort'):
        processedData = 1.0 - rankSortData(rawData)
    elif(args.process == 'minmax'):
        processedData = minMaxNormalization(rawData)
    elif(args.process == '1-minmax'):
        processedData = 1.0 - minMaxNormalization(rawData)
    elif(args.process == '1-values'):
        processedData = 1.0 - rawData
    elif(args.process == 'zscore'):
        processedData = zscore(rawData)
    elif(args.process == 'negzscore'):
        processedData = -1.0*zscore(rawData)
    elif(args.process == 'attenuate'):
        processedData = attenuateEndPoints(rawData)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        sys.exit(-1)

    print("@> Processing the data started!")
    with open(args.outfile, 'w') as f:
        #f.write("#Resid Value\n")
        for i in range (len(processedData)):
            f.write("{:} {:6.2f}\n".format(data.T[0][i], processedData[i]))
    print("@> Processing the data finished successfully!")
    sys.exit()

