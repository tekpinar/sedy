import sys
from prody import *
import numpy as np

from sedy.postprocess import *

one_letter ={'VAL':'V', 'ILE':'I', 'LEU':'L', 'GLU':'E', 'GLN':'Q', \
            'ASP':'D', 'ASN':'N', 'HIS':'H', 'TRP':'W', 'PHE':'F', 'TYR':'Y',    \
            'ARG':'R', 'LYS':'K', 'SER':'S', 'THR':'T', 'MET':'M', 'ALA':'A',    \
            'GLY':'G', 'PRO':'P', 'CYS':'C'}
#  Amino acid charge.
chargeDict = {'A': 0, 'C': 0, 'D': -1, 'E': -1,
            'F': 0, 'G': 0, 'H': 0, 'I': 0,
            'K': 1, 'L': 0, 'M': 0, 'N': 0,
            'P': 0, 'Q': 0, 'R': 1, 'S': 0,
            'T': 0, 'V': 0, 'W': 0, 'Y': 0}

def minMaxNormalization(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

def chargeApp(args):

    #Parse the protein structures.
    structure = parsePDB(args.pdb)
    calphas = structure.select('name CA')
    N = calphas.numAtoms()
    
    chidsList = calphas.getChids()
    resnumsList = calphas.getResnums()
    
    print("@> Assigning charges to amino acids using predefined values at pH 7.4 values!")
    charge = N*[None]
    for i in range (N):
        # print(calphas[i].getResname())
        charge[i] = chargeDict[one_letter[calphas[i].getResname()]]


    if(args.process == 'minmax'):
        charge = minMaxNormalization(charge)
    elif(args.process == '1-minmax'):
        charge = 1.0 - minMaxNormalization(charge)
    elif(args.process == '1-values'):
        charge = 1.0 - charge
    elif(args.process == 'ranksort'):
        charge = rankSortData(charge)
    elif(args.process == '1-ranksort'):
        charge = 1.0 - rankSortData(charge)
    elif(args.process == 'zscore'):
        charge = zscore(charge)
    elif(args.process == 'negzscore'):
        charge = -1.0*zscore(charge)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        print("@> Available postprocessing options:")
        print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
        sys.exit(-1)

    if(args.outfile != ""):
        if(args.outfile.lower().endswith(('.pdb'))):
            calphas.setBetas(charge)
            writePDB(args.outfile, calphas)
        else:
            with open(args.outfile, 'w') as file:
                file.write("#Resid Value Chain\n")
                for i in range (N):
                    file.write("{:d} {:.3f} {:s} \n".format(resnumsList[i], \
                                                            charge[i], \
                                                            chidsList[i]))

    print("@> Finished the task of charge assignments!")
