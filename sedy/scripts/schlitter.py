from prody import *
import numpy as np
import MDAnalysis as mda
from MDAnalysis.analysis import align
#from MDAnalysis.analysis.encore.covariance import ml_covariance_estimator
from sedy.postprocess import *
from collections import OrderedDict

resName2Mass = {"ALA":71.0779,
                "ARG":156.1857,
                "ASN":114.1026,
                "ASP":115.0874,
                "ASH":115.0874, #Amber nomenclature for neutral ASP. Molecular mass?
                "CYS":103.1429, #Usual Cysteine in PDB
                "CYX":103.1429, #Amber nomenclature for cysteines. Molecular mass? 
                "CYM":103.1429, #Amber nomenclature for cysteines. Molecular mass? 
                "GLN":128.1292,
                "GLU":129.1140,
                "GLH":129.1140, #Amber nomenclature for neutral GLU. Molecular mass?
                "GLY":57.0513,
                "HIS":137.1393,
                "HSD":137.1393, #Charmm nomenclature. In fact, molecular mass is not same for all. 
                "HSE":137.1393, #Charmm nomenclature. In fact, molecular mass is not same for all. 
                "HSP":137.1393, #Charmm nomenclature. In fact, molecular mass is not same for all. 
                "HID":137.1393, #Amber nomenclature. In fact, molecular mass is not same for all. 
                "HIE":137.1393, #Amber nomenclature. In fact, molecular mass is not same for all. 
                "HIP":137.1393, #Amber nomenclature. In fact, molecular mass is not same for all.                
                "ILE":113.1576,
                "LEU":113.1576,
                "LYS":128.1723,
                "MET":131.1961,
                "PHE":147.1739,
                "PRO":97.1152,
                "SER":87.0773,
                "THR":101.1039,
                "TRP":186.2099,
                "TYR":163.1733,
                "VAL":99.1311}

aaDict = OrderedDict({'A':'ALA',
                 'C':'CYS', 
                 'D':'ASP', 
                 'E':'GLU',
                 'F':'PHE',
                 'G':'GLY',
                 'H':'HIS',
                 'I':'ILE', 
                 'K':'LYS',
                 'L':'LEU',
                 'M':'MET',
                 'N':'ASN',
                 'P':'PRO',
                 'Q':'GLN',
                 'R':'ARG',
                 'S':'SER',                  
                 'T':'THR',
                 'V':'VAL',
                 'W':'TRP', 
                 'Y':'TYR'})
# def minMaxNormalizetion(data):
#     return (data - np.min(data)) / (np.max(data) - np.min(data))

def calculateSchlitter(N, resnamesList, covMatrix, \
    normalized=True, outfile="schlitter-entropy-per-residue.dat"):
    """
        Calculate Schlitter entropy per residue
        from a coarse-grained elastic network model.

        N: Number of calpha atoms 
        resnamesList: a list of resnames.
        covMatrix: Covariance Matrix

    Return:
    ------
        schlitterList: an numpy array    
        The array has length N and contains Schlitter entropy values for all aa's.  

    """

    M = np.zeros((3, 3))
    I = np.zeros((3, 3))
    np.fill_diagonal(I, 1.0)

    #coeff1 = 1.0 #kTe^2/(hbar^2)
    coeff1 = 45.7061082138 #In fact, this is the real value.
    # obtained as a result of the following operation
    # (4.11×10^(−21)*e*e/(1.054571817×10^(-34))^2)*1.67377 x 10^(-27)*10^(-20)
    # in metric units. Metre, Kilogram, seconds, joules etc.

    coeff2 = 1.0 #0.5*k

    #for i in range (N):
    schlitterList = []
    for i in range (N):
        # print(resName2Mass[calphas[i].getResname()])
        np.fill_diagonal(M, resName2Mass[resnamesList[i]])
        sigma = covMatrix[i*3:(i+1)*3, i*3:(i+1)*3]
        detValue = np.linalg.det(np.add(I, coeff1*np.dot(M, sigma)))
        schlitterList.append(coeff2*np.log(detValue))

    # import matplotlib.pyplot as plt
    # plt.figure()
    # plt.plot(1.0 - np.array(schlitterList))
    # plt.show()
        #print(sigma)
    # if(normalized):
    #     schlitterList = minMaxNormalizetion(schlitterList)

    return np.array(schlitterList)

def schlitterApp(args):

    # parser = argparse.ArgumentParser()
    # parser.add_argument("-p", "--pdbfile", dest="pdbfile", required=True, default=None)
    # parser.add_argument("-n", "--nmodes", dest="nmodes", type=str,  required=False, default=None)
    # parser.add_argument("-o", "--outfile", dest="outfile", default="outfile.dat")
    # args = parser.parse_args()

    if(args.trajectory == None):
        #Parse the protein structures.
        structure = parsePDB(args.pdb)
        calphas = structure.select('name CA')
        N = calphas.numAtoms()
        resnamesList = calphas.getResnames()
        chidsList = calphas.getChids()
        resnumsList = calphas.getResnums()
        #Calculate Covariance
        anm = ANM('ANM')
        anm.buildHessian(calphas)
        if(args.nmodes==None):
            anm.calcModes(n_modes='all')
        else:
            anm.calcModes(n_modes=int(args.nmodes))
        covMatrix = calcCovariance(anm)
    else:
        print("@> Calculating the covariance matrix using the molecular dynamics trajectory!")
        # Create the universe (That sounds really fancy :)
        universe = mda.Universe(args.pdb, args.trajectory)

        # Create an atomgroup from the alpha carbon selection
        calphas = universe.select_atoms("protein and name CA")        
        N = calphas.n_atoms

        #Align the trtajectory
        alignTrajectory = True
        # Perform Calpha alignment first
        if alignTrajectory:
            print("@> Aligning only Calpha atoms to the initial frame!")
            alignment = align.AlignTraj(universe, universe, select="protein and name CA", in_memory=False)
            alignment.run()
        
        chidsList = calphas.chainIDs
        resnumsList = calphas.resnums
        resnamesList = calphas.resnames

        Rvector = []
        skip = 1
        # Iterate through the universe trajectory
        for timestep in universe.trajectory[args.beg:args.end:skip]:
            Rvector.append(calphas.positions.flatten())
        ##############################################

        # I reassign this bc in lmiMatrix calculation, we may skip some part of the trajectory!
        N_Frames = len(Rvector)

        R_average = np.mean(Rvector, axis=0)
        print("@> Calculating covariance matrix from MD trajectory:")

        ################### Covariance matrix calculation part!    
        fullCovarMatrix = np.zeros((3 * N, 3 * N), np.double)
        for k in range(0, len(Rvector)):
            if k % 100 == 0:
                print("@> Frame: " + str(k))
            deltaR = np.subtract(Rvector[k], R_average)
            fullCovarMatrix += np.outer(deltaR, deltaR)

        # Do the averaging
        covMatrix = fullCovarMatrix / float(N_Frames)

    print("@> Calculating Schlitter  entropy!")

    # Calculate Schlitter Per Residue
    schlitter = calculateSchlitter(N, resnamesList, covMatrix, 
                        normalized=True, outfile=args.outfile)

    print("@> Finished Schlitter  entropy calculation!")

    if(args.process == 'minmax'):
        schlitter = minMaxNormalization(schlitter)
    elif(args.process == '1-minmax'):
        schlitter = 1.0 - minMaxNormalization(schlitter)
    elif(args.process == '1-values'):
        schlitter = 1.0 - schlitter
    elif(args.process == 'ranksort'):
        schlitter = rankSortData(schlitter)
    elif(args.process == '1-ranksort'):
        schlitter = 1.0 - rankSortData(schlitter)
    elif(args.process == 'zscore'):
        schlitter = zscore(schlitter)
    elif(args.process == 'negzscore'):
        schlitter = -1.0*zscore(schlitter)
    elif(args.process == None):
        print("@> No postprocessing applied!")
    else:
        print("@> Unknown postprocessing option!")
        print("@> Available postprocessing options:")
        print("@> minmax, 1-minmax, 1-values, ranksort, 1-ranksort, zscore, negzscore")
        sys.exit(-1)

    if(args.outfile != ""):
        with open(args.outfile, 'w') as file:
            file.write("#Resid Value Chain\n")
            for i in range (N):
                file.write("{:d} {:.6f} {:s} \n".format(resnumsList[i], \
                                schlitter[i], chidsList[i]))
    print("@> Finished Schlitter entropy calculation!")
