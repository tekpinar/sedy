import sys
import numpy as np
from scipy.stats import rankdata,zscore
import matplotlib.pyplot as plt

def rankSortData(dataArray):
    """
        This function ranksorts protein data and converts it
        to values between [0,1.0].
    
    Parameters:
    ----------
    dataArray: numpy array of arrays
               data read by numpy.loadtxt

    Returns:
    -------
    normalizedRankedDataArray: numpy array
    """
    normalizedRankedDataArray = rankdata(dataArray)/float(len(dataArray))

    return (normalizedRankedDataArray)

def minMaxNormalization(data):
    """
        Min-max normalization of a data array.

    Parameters:
    ----------
    data: numpy array
          

    Returns:
    -------
    normalizeddata: numpy array
    """
    return (data - np.min(data)) / (np.max(data) - np.min(data))

def zscore(data):
    """
        Z-score normalization

    Parameters:
    ----------
    data: numpy array
          

    Returns:
    -------
    normalizeddata: numpy array

    """
    return (data - np.mean(data))/np.std(data)

def attenuateEndPoints(data):                                                                                                    
    """                                                                                                                         
        Reduce signal strength of the signal on N and C terminals
        with a sigmoid function.                                                                
    """                                                                                                                         
                                                                                                                                
    debug = 0                                                                  
                                                                               
    n = len(data)                                                              
    if(debug):                                                                 
        #Check array lengths                                                   
        print(n/2)                                                             
        print(len(data))

    if(n%2 == 0):                                                              
        x1 = np.linspace(-0, 10, int(n/2))                                     
        x2 = np.linspace(-10, 0, int(n/2))                                     
    else:                                                                      
        x1 = np.linspace(-0, 10, int((n-1)/2))                                 
        x2 = np.linspace(-10, 0, int((n+1)/2))                                 
                                                                               
    z1 = 1/(1 + np.exp(-2.50*x1))                                              
    z2 = 1.0 - (1/(1 + np.exp(-2.50*x2)))                                      
                                                                               
                                                                               
    z = np.append(z1, z2)
    if(debug):                                                                 
        #Check array lengths                                                   
        print(len(z1))                                                     
        print(len(z2))                                                         
        print(len(z))                                                          
        #Plot the arrays                                                       
        plt.plot(z)                                                            
        plt.plot(data)                                                         
        plt.plot(data*z)                                                       
        plt.xlabel("x")                                                        
        plt.ylabel("Sigmoid(X)")                                               
        plt.show()

    if(len(data) == len(z)):                                                   
        return (data*z)                                                                                                          
    else:                                                                       
        print("ERROR: Can not attenuate N and C terminal data signals!")        
        sys.exit(-1)
        
