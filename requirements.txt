numpy<1.24
scipy<1.11.0
matplotlib<3.9.0
pandas
prody
correlationplus>=0.2.0
mdanalysis>=2.3.0
