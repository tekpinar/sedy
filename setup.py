
###############################################################################
# sedy - A Python toolkit to investigate relations between protein sequences  #
#          and dynamics.                                                      #
#                                                                             #
# Authors: Mustafa Tekpinar                                                   #
# Copyright Mustafa Tekpinar 2022                                             #
#                                                                             #
# This file is part of sedy.                                                  #
#                                                                             #
# sedy is free software: you can redistribute it and/or modify                #
# it under the terms of the GNU Lesser General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# sedy is distributed in the hope that it will be useful,                     #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU LESSER General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with sedy.  If not, see <https://www.gnu.org/licenses/>.              #
###############################################################################

from setuptools import setup, find_packages

from sedy import __version__ as cp_vers

setup(name='sedy',
      version=cp_vers,
      description="A Python toolkit to investigate relations between protein sequences and dynamics.",
      long_description=open('README.md').read(),
      long_description_content_type="text/markdown",
      author="Mustafa Tekpinar",
      author_email="tekpinar@buffalo.edu",
      url="https://github.com/tekpinar/sedy",
      download_url="https://github.com/tekpinar/sedy",
      license="LGPL",
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Operating System :: POSIX',
          'Operating System :: Microsoft :: Windows',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.7',
          'Programming Language :: Python :: 3.8',
          'Programming Language :: Python :: 3.9',
          'License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)',
          'Intended Audience :: Science/Research',
          'Topic :: Scientific/Engineering :: Bio-Informatics',
          'Topic :: Scientific/Engineering :: Chemistry',
          'Topic :: Scientific/Engineering :: Physics'
          ],
      python_requires='>=3.6',
      install_requires=[i for i in [l.strip() for l in open("requirements.txt").read().split('\n')] if i],
      # zip_safe=False,
      packages=[p for p in find_packages() if p != 'tests'],
      # file where some variables must be fixed by install
      entry_points={
          'console_scripts': [
              'sedy=sedy.scripts.sedy:main'
          ]
      },
      include_package_data=True,
      package_data={'sedy':['matrix/*', 'distributions/*']},
      )
