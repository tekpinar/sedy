# sedy

*A Python toolkit to investigate relations between protein sequences and dynamics.*

sedy contains five main scripts: plots, dfi, convert, compare and removegaps. 

## Installation
Download the repository:
```bash
git clone https://gitlab.com/tekpinar/sedy.git
```


Go to the sedy folder. Then, run the following command:

```bash
pip install -e .
```


## Quick Start

You can invoke help about each sedy script as follows:

sedy dfi -h
sedy plots -h
sedy convert -h
sedy compare -h
sedy removegaps -h

sedy dfi tool calculates dfi. 

sedy plots tool extracts data (such as averages etc) from the text data output of GEMME or FoldX.
It is supposed to be a 2D plots tool.

sedy convert will convert text output from (to) GEMME to (from) FoldX and Rhapsody formats. 

sedy compare will give you Spearman correlation between two DMS data sets.  

## Cite
We recommend you to put a link to the github repository for now. 

## Licensing

*sedy* is developed and released under [GNU Lesser GPL Licence](https://www.gnu.org/licenses/lgpl-3.0.en.html). 
Please read to the **COPYING** and **COPYING.LESSER** files to know more. 
