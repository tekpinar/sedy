#!/bin/bash

#Basic usage from just a pdb file and coarse-grained elastic network model.  
sedy dfi -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_dfi.dat
sedy dfi -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_dfi.dat

sedy schlitter -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_schlitter.dat
sedy schlitter -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_schlitter.dat

sedy rmsf -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_rmsf.dat
sedy rmsf -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_rmsf.dat

sedy cfb -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_cfb --nmodes 100
sedy cfb -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_cfb --nmodes 100

#Advanced usage
#Postprocess DFI: Ranksorting data and subtracting all values from 1.0

sedy dfi -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_dfi_1-ranksorted.dat --process 1-ranksort

sedy dfi -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_dfi_1-ranksorted.dat --process 1-ranksort

#Postprocess RMSF data and use only 100 modes
sedy rmsf -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_rmsf_1-minmax-100.dat --process 1-minmax -n 100
sedy rmsf -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_rmsf_1-minmax-100.dat --process 1-minmax -n 100


#Current flow betweenness with optimized parameters.
#If nmodes is not specified, all modes are considered.
#When you consider all modes, correlations may reduce. Therefore, a
#lower vmin value may be necessary. Never use vmin 0.0 due to logarithmic
#nature of the edge weights in correlationplus. 
sedy cfb -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_cfb  --vmin 0.05 --dmax 10.0
sedy cfb -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_cfb --vmin 0.05 --dmax 10.0


#RMSF analysis from an MD trajectory
sedy rmsf -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_rmsf-md.dat -t ../data/6y2e_monomer_no_ligand_protein_sim1.xtc

sedy rmsf -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_rmsf-md.dat -t ../data/6y2e_dimer_no_ligand_protein_sim1.xtc

#CFB analysis from an MD trajectory
sedy cfb -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -o 6y2e_monomer_no_ligand_protein_sim1_cfb  --vmin 0.05 --dmax 10.0 -t ../data/6y2e_monomer_no_ligand_protein_sim1.xtc 
sedy cfb -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -o 6y2e_dimer_no_ligand_protein_sim1_cfb  --vmin 0.05 --dmax 10.0 -t ../data/6y2e_dimer_no_ligand_protein_sim1.xtc

#Schlitter entropy from an MD trajectory
sedy schlitter -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -t ../data/6y2e_monomer_no_ligand_protein_sim1.xtc -o 6y2e_monomer_no_ligand_protein_sim1_schlitter_md.dat

sedy schlitter -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -t ../data/6y2e_dimer_no_ligand_protein_sim1.xtc -o 6y2e_dimer_no_ligand_protein_sim1_schlitter_md.dat


#A very crude DFI calculation from an MD trajectory
sedy dfi -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb -t ../data/6y2e_monomer_no_ligand_protein_sim1.xtc -o 6y2e_monomer_no_ligand_protein_sim1_dfi_md.dat --process 1-ranksort
sedy dfi -p ../data/6y2e_dimer_no_ligand_protein_sim1.pdb -t ../data/6y2e_dimer_no_ligand_protein_sim1.xtc -o 6y2e_dimer_no_ligand_protein_sim1_dfi_md.dat --process 1-ranksort

#Get Bfactors from a file and perform postprocess like zscore normalization!
sedy bfactor -p ../data/6y2e_monomer_no_ligand_protein_sim1.pdb --process zscore -o bfactor-zscores.dat

#Calculate Jensen-Shannon divergence-conservation
#The last -g 0.99999 parameter prevents -1000 values in the JS divergence scores. 
sedy conservation -s js_divergence -o BLAT-js-conservation.scores ../data/aliBLAT_processed.fasta -g 0.99999
